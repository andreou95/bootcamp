<?php

namespace App\Http\Controllers;
use App\Team;

use Illuminate\Http\Request;

class TeamController extends Controller
{
  
   //show all teams
   public function index(Request $Request)
   {
   		$teams= Team::all();
   		return $teams;
   }
   //show a team
   public function show(Request $Request,$team)
   {
   		// $teamModel=Team::find($team);

   		// return $teamModel;

   		return $team->players;
   }
   //edit a team - show the edit form
   public function edit(Request $Request,Team $team)
   {
   		return 'edit'.$team;
   }
   //update a team -handle the edit form
   public function update(Request $Request,$team)
   {
   		return "update".$team;
   }
   //destroy team
   public function destroy(Request $Request,$team)
   {
   		return 'destroy';
   }





}
