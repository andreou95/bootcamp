<?php

namespace App\Http\Controllers;

use App\Player;

use Illuminate\Http\Request;

class PlayerController extends Controller
{	
   //show all players
   public function index(Request $Request)
   {
   		$players= Player::all();
   		// return $players;
   		return view('players.index',compact('players'));
   }
   //show a player
   public function show(Request $Request, Player $player)
   {
   		// $playerModel=Player::find($player);

   		// return $playerModel;

   		// return $player->name;

   	return view('players.show',compact('player'));
   }
   //edit a player - show the edit form
   public function edit(Request $Request,Player $player)
   {
   		return view('players.edit');

   }
    //update a player -handle the edit form
   public function update(Request $Request,$player)
   {
   		return "update".$player;
   }
   //destroy player
   public function destroy(Request $Request,$player)
   {
   		return 'destroy';
   }
}
