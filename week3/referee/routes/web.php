<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/getReferee','refereeController@getReferee');

//player actions

//index of all players
Route::get('/player','PlayerController@index');

//route to show a specific player
//{} indicates a route parameter (route parameters)
Route::get('/player/{player}','PlayerController@show');

//edit route for a player -show edit form
Route::get('/players/{player}/edit','PlayerController@edit');

//update route -handling the submission from the edit form
Route::put('/players/{player}','PlayerController@update');

//delete route - remove the model from the db
Route::delete('/players/{player}','PlayerController@destroy');

//TEAM
//index of all players
Route::get('/team','teamController@index');

//route to show a specific player
//{} indicates a route parameter (route parameters)
Route::get('/team/{team}','teamController@show');

//edit route for a player -show edit form
Route::get('/teams/{team}/edit','teamController@edit');

//update route -handling the submission from the edit form
Route::put('/teams/{team}','teamController@update');

//delete route - remove the model from the db
Route::delete('/teams/{team}','teamController@destroy');


