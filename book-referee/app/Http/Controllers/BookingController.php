<?php

namespace App\Http\Controllers;
use App\Referee;
use App\Booking;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {



        clock()->startEvent('call',"DB load");
        
        $referees= Referee::all();
        clock()->endEvent('call');
        return view('bookings.index',compact('referees'));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Referee $referee)
    {

        $booking = $referee->bookings()->create([
            'user_id'=> Auth::user()->id,
            'date' => $request->get('date')
        ]);
 
        return redirect()->route('mybookings',$referee);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,Referee $referee)
    {
        // $referee=Referee::find()->get();
        // foreach ($referees as $referee ) {
            
        // }

        return view('bookings.show',compact('referee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }

    public function authIndex()
    {

        $user=Auth::user();

        // // return view('');

        // return $user->bookings;

        // return $referee->bookings;

        // $user=$user->bookings;
        return view('bookings.mybookings',compact('user'));
        // return view('mybookings');



    }
}
