<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Referee;

class RefereeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // clock()->startEvent('call-call',"DB load");
        
        $referees= Referee::all();
        
        // $referees=Referee::with('reviews.referees');
        return view('referees.index',compact('referees'));

        // clock()->endEvent('call-call');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
        return view('referees.register');
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $referee= new Referee([
            'name'=> $request->get('exampleInputName'),
            'email'=>$request->get('InputEmail'),
            'phone'=>$request->get('InputPhone'),
            'certificate'=>$request->get('certificate'),
            'sport'=>$request->get('sports'),
            'location'=>$request->get('location'),
            'price'=>$request->get('price')
        ]);

        $referee->save();

        return redirect('/referees/register')->with('success','Added referee');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Referee  $referee
     * @return \Illuminate\Http\Response
     */
    public function show(Request $Request,Referee $referee)
    {
   
     return view('referees.show',compact('referee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Referee  $referee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $referee=Referee::find($id);
        return view('referees.edit',compact('referee'));
    
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Referee  $referee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        $referee = Referee::find($id);
       
        $referee->name = $request->get('edit_name');
        $referee->email= $request->get('edit_email');
        $referee->phone = $request->get('edit_phone');
        $referee->certificate = $request->get('edit_certificate');
        $referee->sport = $request->get('edit_sports');
        $referee->location = $request->get('edit_location');
        $referee->price = $request->get('edit_price');


       
        $referee->save();

        return view('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Referee  $referee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Referee $referee)
    {
        //
    }
     public function login(Request $request){

        return view('referees.login');
     }
    

  }
