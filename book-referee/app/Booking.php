<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Referee;
use App\User;

class Booking extends Model
{
    protected $table='bookings';

    protected $fillable = [
        'date','user_id','referee_id',
    ];

    public function referee()
    {
    	return $this->belongsTo(Referee::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
