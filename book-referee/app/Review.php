<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Referee;

class Review extends Model

{
	protected $table='reviews';
	
    public function referee()
    {

	return $this->belongsTo(Referee::class);   
	}
}
