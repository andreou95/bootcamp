<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Review;

class Referee extends Model
{
    protected $fillable=[
    	'name',
       	'phone',
       	'email',
       	'certificate',
       	'sport',
       	'location',
       	'price',

       ];

       public function reviews(){

       	return $this->hasMany(Review::class);
       }

       public function bookings()
       {

        return $this->hasMany(Booking::class);

       }
}


