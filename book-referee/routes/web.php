<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/',function(){
	return view('/home');
});

Route::get('/referees','RefereeController@index');

Route::get('referees/login','RefereeController@login');

Route::get('/referees/register','RefereeController@create');

//{} indicates a route parameter (route parameters)
Route::get('/referees/{referee}','RefereeController@show')->name('referees.show');
//can name the routes like this way->name('detailView');

//edit route for a referee -show edit form
Route::get('/referees/{referee}/edit','RefereeController@edit')->name('referees.edit');

//update route -handling the submission from the edit form
Route::patch('/referees/{referee}','RefereeController@update')->name('referees.update');

//delete route - remove the model from the db
Route::delete('/referees/{referee}','RefereeController@destroy');


Route::post('/submit','RefereeController@store');






//REVIEWS

Route::get('/reviews','ReviewController@index');

//{} indicates a route parameter (route parameters)
Route::get('/reviews/{review}','ReviewController@show');

//edit route for a referee -show edit form
Route::get('/reviews/{review}/edit','ReviewController@edit');

//update route -handling the submission from the edit form
Route::put('/reviews/{review}','ReviewController@update');

//delete route - remove the model from the db
Route::delete('/reviews/{review}','ReviewController@destroy');



//Bookings


Route::get('/bookings','BookingController@index');

//{} indicates a route parameter (route parameters)
Route::get('/bookings/{referee}','BookingController@show');

Route::post('/confirm/{referee}','BookingController@store')->name('booking.store');

Route::get('/mybookings/{user}','BookingController@authIndex')->name('mybookings');



Route::get('/home', 'HomeController@index')->name('home');


//AUTH
Auth::routes();
