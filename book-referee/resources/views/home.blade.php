@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"style='background-color: #8380B6';>Welcome {{Auth::user()->name }}</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <!-- <h1>Book-A-Ref</h1>
                    <h1>Welcomes you!!<h1><h2>{{Auth::user()->name }} </h2>
                    <p>Hiring a referee couldn't be more easier</p>              -->
                        
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 " >
                    <img class="rounded" src="{{url('/image/referees2.jpg')}}" alt="//" >
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 ">
                    <h4 class="title mb-2 h1">What we do</h4>
                    <p class="mb-4">Hire a Referee for any event or fundraiser.</p>
                    <ul class="list-unstyled">
                        <li>Read reviews for each referee.</li>
                        <li>Ability to signup as a referee .</li>
                        <li>Choose the date you want to book.</li>
                       
                        <li>Some quick example text to build on the card title and make up the bulk of the card's content.</li>
                    </ul>
                </div>
            </div>
        </div>
 
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
