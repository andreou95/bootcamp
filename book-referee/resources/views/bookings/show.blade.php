@extends('layouts.app')
@section('content')


<!-- @if(\Session::has('success'))
<div class="alert alert-success" role="alert">
  <p>{{\Session::get('success')}}</p>
  <a href="/home" class="btn btn-primary">Back</a>
</div>

@endif -->
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            	<!-- {{$user = Auth::user()}} -->
                <div class="panel-heading">Bookings for <strong>{{$user->name}}</strong></div>

                <div class="panel-body">
                  
        <form action="{{route('booking.store',$referee->id)}}" method='POST'>
        {{csrf_field()}}
     
     <table class="table table-light">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Price</th>
      <th scope="col">Location</th>
      <th scope="col">certification</th>
      <th scope="col">Date</th>
      <th scope="col">Actions</th>


    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">{{$referee->id}}</th>
      <td>{{$referee->name}}</td>
      <td>${{$referee->price}}</td>
      <td>{{$referee->location}}</td>
      <td>{{$referee->certificate}}</td>
      <td><input type="date" name="date"></td>
      <td>

       
      
      <input type='submit' class='btn btn-primary btn-sm' value='Confirm'>
       


</td>
  
  </tbody>
</table>
</form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
