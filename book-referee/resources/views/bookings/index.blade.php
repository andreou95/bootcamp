@extends('layouts.app')
@section('content')
<br>
<div class="container">
        <div class="row" >
            @foreach($referees as $referee)
            <div class="col-md-4">
            	
              
                @include('referees/referee-info')
            </div>
            @endforeach
        </div>
    </div>
@endsection