@extends('layouts.app')
@section('content')


<div class="container">
        <div class="row" >
        	
<?php $total=0 ?>

@foreach($user->bookings as $booking)

<!-- <h1>Bookings made by:<strong>{{$booking->user->name}}</strong></h1>
 -->
            	
<div class="col-md-4">
              
<div class="card text-primary border-primary" style="width: 16rem; margin-top: 15px ;margin-bottom: 15px;">
	
		<h2 class="card-header">{{$booking->referee->name}}</h2>
	<!-- <div class="card-img-overlay">
		<img src="{{$booking->referee->profile}}" class="card-img-top" alt="..." style='opacity: 0.5; filter:alpha(opacity=50);'>
	</div> -->
	<div class="card-body">

		<h5>Sport: {{$booking->referee->sport}}</h5>
		<p class='card-text'>Location: {{$booking->referee->location}}</p>
		<p class='card-text'>Price: {{$booking->referee->price}}</p>
		<p class='card-text'>Date: {{$booking->date}}</p>

		<?php $total+= $booking->referee->price ?>

</div>
</div>
</div>




@endforeach

<table>
	<tr>
		
			<td >total spent $
				<h1 id="total_price-{{ $total }}" ></h1>
			</td>
	</tr>
</table>

			
<script>
document.getElementById('total_price-{{ $total }}').innerHTML = {{$total}};
</script>

@endsection