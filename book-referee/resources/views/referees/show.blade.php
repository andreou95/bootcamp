@extends('layouts.app')

@section('content')

  @if(\Session::has('success'))
<div class="alert alert-success" role="alert">
  <p>{{\Session::get('success')}}</p>
  <a href="/home" class="btn btn-primary">Back</a>
</div>

@endif

	 <div class="container">
    <div class='row'>
     <div class="card text-center">
      
      <div class="card-header">

          <h1>{{$referee->name}}</h1>
          </div>
          <div class="card-body">

          <p><i class="glyphicon glyphicon-envelope"><strong>Email: </strong></i> {{$referee->email}}</p>
          <p><strong>Certification: </strong>{{$referee->certificate}}</p>
          <p><strong>Sport: </strong>{{$referee->sport}}</p>
          <p><strong>Phone: </strong>{{$referee->phone}}</p>
          <p><strong>Location: </strong>{{$referee->location}}</p>
          <p><strong>Price: $<strong>{{$referee->price}}</p>
          </div>
          <div class="card-footer">
          <p><a class="btn btn-primary btn-lg" href="/bookings" role="button">Back</a>
          <a class="btn btn-primary btn-lg" href="/bookings/{{$referee->id}}" role="button">Hire me</a>

          <a class="btn btn-info btn-lg" 
          href="{{ route('referees.edit',$referee->id) }}" role="button">Edit</a>
          
          <a class="btn btn-primary btn-lg"  role="button" onclick="myFunction()">Show Reviews<span class="badge badge-light">{{ $referee->reviews()->count() }}</span></a></p>
        </div>
          </div>
            </div>
        

</div>


  <script type="text/javascript">
    
    function myFunction() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
  </script>

  
    
<div class='container'>
<div id="myDIV" style="display: none; background-color: #8789C0">
  <div class='jumbotron text-center'>
  <h1>READ WHAT SOME OF OUR CUSTOMERS ARE SAYING</h1>



            @foreach($referee->reviews as $review) 
          
            <!--  <i class="fa fa-quote-left" aria-hidden="true"></i><h2>{{$review->title}}</h2><i class="fa fa-quote-left" aria-hidden="true"></i>
             <p>Description: {{$review->description}}</p> -->
             <div class="col-md-4">
              
<div class="card text-danger border-primary" style="width: 16rem; margin-top: 15px ;margin-bottom: 15px;">
  
    <h2 class="card-header" style="background-color: #111D4A">{{$review->title}}</h2>
 
     <div class="card-body" style="background-color: #111D4A">

    
    <p class='card-text'> {{$review->description}}</p>
   

</div>
</div>
</div>
    
          @endforeach
        </div>
      </div>
    </div>

@endsection