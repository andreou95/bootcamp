@extends('layouts.app')
@section('content')

@if(\Session::has('success'))
<div class="alert alert-success" role="alert">
  <p>{{\Session::get('success')}}</p>
  <a href="/home" class="btn btn-primary">Back</a>
</div>

@endif

  <div class='container'>
    <div class="container">
  <h2>Become a Referee</h2>

   <form id="register_form" onsubmit="return validateForm()" method='POST'action="/submit">

    {{csrf_field()}}

  <div class="form-group">
    <label for="exampleInputName">Name</label>
    <input type="text" class="form-control" id="exampleInputName" name= 'exampleInputName' placeholder="Full name" >
  </div>
  <div class="form-group">
    <label for="InputEmail">Email address</label>
    <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter email" name="InputEmail">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>



  <div class="form-group">
    <label for="InputPhone">Phone</label>
    <input type="string" class="form-control" id="InputPhone" name="InputPhone" placeholder="Phone number">
  </div>

<div class="form-group">
  <label for="certificates">Select Certificate</label>
  <select class="form-control" id="certificate" name="certificate">
    <option value="beginner">beginner</option>
  <option value="intermediate">intermediate</option>
  <option value="advanced">advanced</option>
  </select>
</div>

  <div class="form-group">
  <label for="sports">Select Sport</label>
  <select class="form-control" id="sports" name="sports">
  <option value="football">football</option>
  <option value="basketball">basketball</option>
  <option value="golf">golf</option>
  <option value="tennis" >tennis</option>
  <option value="rugby" >rugby</option>
  </select>
</div>




<div class="form-group">
  <label for="location">Select Location</label>
  <select class="form-control" id="location" name="location">
    <option value="Bath">Bath</option>
  <option value="Newport">Newport</option>
  <option value="Cardiff">Cardiff</option>
  <option value="Bristol" >Bristol</option>
  <option value="Swansea" >Swansea</option>
  </select>
</div>

 <div class="form-group">
    <label for="price">Price</label>
    <input type="number" class="form-control" id="price" name="price" placeholder="Price">
  </div>
 
  <br>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div></div>

<script>
function validateForm() {
  var x = document.forms["register_form"]["exampleInputNames"].value.length;
  if (x == 0) {
    alert("Name must be filled out");
    return false;
  }                       
  var y = document.forms["register_form"]["InputPhone"].value;
 
  if(y==""){
    alert("price must be filled out");
    return false;

  }
}
</script>
@endsection