@extends('layouts.app')
@section('content')
  <div class='container'>
   <form id="edit_form" method='post'action="{{ route('referees.update', $referee->id) }}">
     {{csrf_field()}}
     {{method_field('PATCH')}}
   <div class="form-group">
    
    
    <label for="edit_name">Name</label>
    <input type="text" class="form-control" id="edit_name" name= 'edit_name' value="{{old ('name',$referee->name)}}" placeholder="Full name">
  </div>
  <div class="form-group">
    <label for="edit_email">Email address</label>
    <input type="email" class="form-control" id="edit_email" aria-describedby="emailHelp" placeholder="Enter email" name="edit_email" value="{{old ('name',$referee->email)}}">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
 
  <div class="form-group">
    <label for="edit_phone">Phone</label>
    <input type="string" class="form-control" id="edit_phone" name="edit_phone" placeholder="Phone number" value="{{old ('name',$referee->phone)}}">
  </div>

<div class="form-group">
  <label for="edit_certificate">Select Certificate</label>
  <select class="form-control" id="edit_certificate" name="edit_certificate">
    <option value="beginner">beginner</option>
  <option value="intermediate">intermediate</option>
  <option value="advanced">advanced</option>
  </select>
</div>

  <div class="form-group">
    <label for="edit_sports">Select Sport</label>
    <select class="form-control" id="edit_sports" name="edit_sports" >
    <option value="football">football</option>
    <option value="basketball">basketball</option>
    <option value="golf">golf</option>
    <option value="tennis" >tennis</option>
    <option value="rugby" >rugby</option>


    </select>
</div>

<div class="form-group">
  <label for="edit_location">Select Location</label>
  <select class="form-control" id="edit_location" name="edit_location">
  <option value="Bath">Bath</option>
  <option value="Newport">Newport</option>
  <option value="Cardiff">Cardiff</option>
  <option value="Bristol" >Bristol</option>
  <option value="Swansea" >Swansea</option>
  </select>
</div>

 <div class="form-group">
    <label for="edit_price">Price</label>
    <input type="number" class="form-control" id="edit_price" name="edit_price" placeholder="Price">
  </div>

  <br>
  <button type="submit" class="btn btn-primary">Save changes</button>
</form>
</div>

@endsection