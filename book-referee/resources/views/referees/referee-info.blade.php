
<div class="card" style= "  width:400px">
	
		<h2 class="card-header">{{$referee->name}}</h2>
		<!-- -img-overlay -->
	<div class="card">
		<img src="{{$referee->profile}}" class="card-img-top" alt="..." style='opacity: 0.5; filter:alpha(opacity=50);'>
	</div>
	<div class="card-body">
		<h5>Sport: {{$referee->sport}}</h5>
		<p class='card-text'>Location: {{$referee->location}}</p>
		  <a href="/referees/{{$referee->id}}" class="btn btn-info">Details</a>
    <!-- edit book button later pass referee id as foreign key to bookings table to display the referee in the bookings -->
    <a href="/bookings/{{$referee->id}}" class="btn btn-primary">Book</a>
	

</div>
</div>
