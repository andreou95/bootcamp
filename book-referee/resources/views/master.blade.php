<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
	<!-- <body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
		    <img src="/image/Referee-Icon.png" width="80" height="80" class="d-inline-block align-top" alt="..">
		    
		  </a>
      
      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="/referees"><i class="fa fa-fw fa-home"></i>Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="#"><i class='fa fa-fw fa-book'></i>Book <span class="sr-only">(current)</span></a>
          </li>
          
        </ul>
        <form class="form-inline my-2 my-lg-0">
      <ul class='navbar-nav'>
          <li class="nav-item active">
            <a class="nav-link" href="/referees/login"><i class="fa fa-fw fa-user"></i>Login <span class="sr-only">(current)</span></a>
            <li class="nav-item active">
            <a class="nav-link" href="/referees/register"><i class="fa fa-envelope-o fa-fw"></i>Register <span class="sr-only">(current)</span></a>
          </li>
        </ul>
    </form>
      </div>
    </nav><br>
  </li></ul></form></div></nav></body></html>
 -->
 
  <nav class="navbar navbar-expand-lg navbar-light" style= "background-color: #FF2A6D" >
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="/referees"><i class="fa fa-fw fa-home"></i>Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="/bookings"><i class='fa fa-fw fa-book'></i>Book <span class="sr-only">(current)</span></a>
          </li>
          
        </ul>

    <div class="mx-auto">
      <a class="navbar-brand" href="/referees">
        <img src="https://png2.cleanpng.com/sh/22aff56eed19b073250e90190bf26e06/L0KzQYm3VMA1N5l5fZH0aYP2gLBuTgdpcaR5hNc2YYP2f7TwggRqd58yftH4dHLkfL20kvVnbaNqfZ9sbHnzPbL5lL14cJp4jN5uLUXkc4WAg8Q6bmM4e9UDLkC5R4WBUMM6OWY3Sqo7NkO0Q4qCUsIveJ9s/kisspng-whistle-association-football-referee-clip-art-whistle-5ac47c49f23cc8.0674803915228263139922.png" width="200" height="80" class="d-inline-block align-top" alt=".."><p>Book-A-Ref</p>
       </a>
  </div>
    
    <form class="form-inline my-2 my-lg-0">
       <ul class='navbar-nav'>
          <li class="nav-item active">
            <a class="nav-link" href="/referees/login"><i class="fa fa-fw fa-user"></i>Login <span class="sr-only">(current)</span></a>
            <li class="nav-item active">
            <a class="nav-link" href="/referees/register"><i class="fa fa-envelope-o fa-fw"></i>Register <span class="sr-only">(current)</span></a>
          </li>
        </ul>
    </form>
  </div>
</nav>
		

	


		@yield('content')
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>