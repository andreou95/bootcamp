<?php

use Illuminate\Database\Seeder;
use App\Review;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
		$faker = \Faker\Factory::create();
		
		for($i=0;$i<20;$i++){
       	
       	Review::create([
       	'title' =>$faker->word,
       	'description'=>$faker->word
       	
       ]);
       
    	}
	}
}
