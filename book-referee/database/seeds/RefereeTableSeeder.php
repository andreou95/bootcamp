<?php

use Illuminate\Database\Seeder;
use App\Referee;

class RefereeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
		$faker = \Faker\Factory::create();
		
		for($i=0;$i<10;$i++){
       	
       	Referee::create([
       	'name' =>$faker->name,
       	'phone' =>$faker->numerify('##########'),
       	'email'=>$faker->unique()->safeEmail,
       	'certificate'=>['beginner','Intermediate','advanced'][rand(0,2)],
       	'sport'=>['tennis','golf','american football','basketball','football','rugby'][rand(0,5)],
        'location'=>['Cardiff','Swansea','Bath','Newport','Bristol'] [rand(0,4)],
        'price'=>['15','20','30'] [rand(0,2)]  	
       ]);
       
    	}
	}
}
