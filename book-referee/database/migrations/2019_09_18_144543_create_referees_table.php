<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createRefereesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void\
     */
    public function up()
    {
         Schema::create('referees',function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->bigInteger('phone');
            $table->string('certificate');
            $table->string('sport');
            $table->string('location');
            $table->string('price');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referees');
    
    }
}
