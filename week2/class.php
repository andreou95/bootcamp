<!DOCTYPE html>
<html>
<head>
	<title>Class</title>
</head>
<body>
	<?php
	//This is the mold nothing is created unless called.
	class Person{

		function Person($name,$surname,$age){
			$this->name =$name;
			$this->surname =$surname;
			$this->age =$age;
		}
	}
	//constructing new object
	$person1= new Person("Giorgos","Andreou",12);
	$person2= new Person("Gior","An",14);
	echo($person1 -> name);

	?>


</body>
</html>