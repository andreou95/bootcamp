<?php

class Player
{
	// TODO could this be refactored to a general 'User' class?
	private $name;
	private $age;
	private $team;

	public function __construct($name, $age)
	{
		$this->name = $name;
		$this->age = $age;
		$this->team = null;
	}

	public function setTeam(Team $team)
	{
		$this->team = $team;
	}

	public function getTeam()
	{
		return $this->team;
	}


	public function getName()
	{
		return $this->name;
	}

	public function getAge()
	{
		return $this->age;
	}
}