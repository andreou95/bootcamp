<div class="card">
  <img src="https://picsum.photos/id/300/300" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title"><?= $player->getName()?></h5>
    <p class="card-text"><?=$player->getAge()?> years old</p>
    <a href="#" class="btn btn-primary">Player profile</a>
  </div>
</div>