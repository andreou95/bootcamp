<div class="card">
  <div class="card-body">
    <h4>
    	<?= $team->getName() ?> - stats
    </h4>
    <p>
    	Goals scored: ?
    </p>
    <h5>
    	Players
    </h5>

	<div class="row">
		<?php foreach($team->getPlayers() as $player){?>

		<div class="col-6 col-md-4">
			<?php require('player-info.php') ?>
		</div>
	<?php } ?>

	</div>


  </div>
</div>