<?php

class Game
{
	private $teamAinfo;
	private $teamBinfo;

	public function __construct(Team $teamA, Team $teamB)
	{
		// build the info objects that will represent the teams
		$this->teamAinfo = new TeamGameInfo($teamA);
		$this->teamBinfo = new TeamGameInfo($teamB);
	}

	public function getTeamInfos()
	{
		return [
			$this->teamAinfo,
			$this->teamBinfo
		];
	}
	public function getWinner()
	{

		$nameTeamAGoals=$this->teamAinfo->getNumGoals();
		$nameteamBGoals=$this->teamBinfo->getNumGoals();

		//Assume a draw and reasign the winner in case its not a draw
		$winningTeam= null;
		if($nameTeamAGoals>$nameteamBGoals){

			$winningTeam= $this->teamAinfo->getTeam();
		}else if ($nameteamBGoals>$nameTeamAGoals){

			$winningTeam=$this->teamBinfo->getTeam();

		}
		return $winningTeam;
	}


	/**
	 * @return int the total number of goals scored by all teams
	 */
	public function getNumGoals()
	{
		return $this->teamAinfo->getNumGoals() + $this->teamBinfo->getNumGoals();
	}

	public function addGoal($player)
	{
		// find the team that the player relates to
		$team = $player->getTeam();

		// find the info object for that team in this game
		$teamGameInfo = $this->teamAinfo->isForTeam($team)
			? $this->teamAinfo
			: $this->teamBinfo;

		// use it to record the goal!
		$teamGameInfo->addGoal($player);
	}
}